var MAX_CONTAINT_WIDTH = 70;
var MAX_CONTAINT_HEIGHT = 70;
var PTM_RATIO = 32;
var TIME_TO_ANIM = 5;

var CONTAINER = CONTAINER || {};

var GameLayer = cc.Layer.extend({
	//Box2D initialization
	b2Vec2:Box2D.Common.Math.b2Vec2,
	b2World:Box2D.Dynamics.b2World,
	b2ContactListener:Box2D.Dynamics.b2ContactListener,

    spritePlayer:null,
	spriteBG:null,
	deltaAnchorBGWidth:null,
	isBGReload:false,
	anotherBG:null,
	winSize:null,
	deltaForCollision:null,
	hillsBG:null,
	world:null,
	contactListener:null,

    init:function () {
        this._super();

		this.contactListener = new this.b2ContactListener();
		this.winSize = cc.Director.getInstance().getWinSize();

		//Initialize Box 2D World
        this.world = new this.b2World(new this.b2Vec2(0, -10), true);
        this.world.SetContinuousPhysics(true);
		this.world.SetContactListener(this.contactListener);
		
    	//Player
		this.spritePlayer = new Player(this.world);
		this.spritePlayer.setPosition(cc.p( this.winSize.width/(6 * PTM_RATIO), this.winSize.height/(3 * PTM_RATIO) ));
		this.addChild(this.spritePlayer, 3);

		//Background
		this.spriteBG = new BG("res/HelloWorld.png");
		this.spriteBG.setAnchorPoint( cc.p(0,0) );
		this.deltaForCollision = -this.winSize.width/8;
		this.spriteBG.setPosition(cc.p(this.deltaForCollision,0));
		this.deltaAnchorBGWidth = this.spriteBG.getContentSize().width;
		this.addChild(this.spriteBG, 1);
		//Vetorial Background
		this.hillsBG = new VBackground(this.world, this.winSize.width/4);
		this.hillsBG.setAnchorPoint(cc.p(0,0));
		this.addChild(this.hillsBG, 2);

		//Enemies
		for (var j = 0; j < OBJECTSPOSITIONS[ENEMY].length; j++) {
			var pos = cc.p( OBJECTSPOSITIONS[ENEMY][j][0]/PTM_RATIO, OBJECTSPOSITIONS[ENEMY][j][1]/PTM_RATIO );
			this.enemy = new Enemy(this.world, pos);
			this.enemy.sortColor();
			this.enemy.setPosition(pos);
			this.addChild(this.enemy, 3);
        	CONTAINER.CREATURE.push(this.enemy);
		}

		//Color Beings
		numColorItem = 1;
		for (var j = 0; j < numColorItem; j++) {
			this.colorItem = new ColorItem(this.world);
			this.colorItem.sortColor();
			this.colorItem.setPosition(cc.p(this.winSize.width/PTM_RATIO, this.winSize.height/(2.5 * PTM_RATIO)));
			this.addChild(this.colorItem, 3);
        	CONTAINER.CREATURE.push(this.colorItem);

		}

		//Schedule
        this.scheduleUpdate();
		this.movingBackground();
		this.schedule(this.movingBackground, TIME_TO_ANIM);
		

        this.setTouchEnabled(true);
		this.setKeyboardEnabled(true);
        return true;
    },
    onTouchesBegan:function (touches, event) {
    },
    onTouchesMoved:function (touches, event) {
    },
    onTouchesEnded:function (touches, event) {
    },
    onTouchesCancelled:function (touches, event) {
    },
	onKeyUp:function (e) {

    },
    onKeyDown:function (e) {
        this.spritePlayer.handleKey(e);
    },
	update:function (dt) {

		//It is recommended that a fixed time step is used with Box2D for stability
        //of the simulation, however, we are using a variable time step here.
        //You need to make an informed choice, the following URL is useful
        //http://gafferongames.com/game-physics/fix-your-timestep/
        var velocityIterations = 8;
        var positionIterations = 1;

        // Instruct the world to perform a single step of simulation. It is
        // generally best to keep the time step and iterations fixed.
        this.world.Step(dt, velocityIterations, positionIterations);
		this.world.ClearForces();

        //Iterate over the bodies in the physics world
        for (var b = this.world.GetBodyList(); b; b = b.GetNext()) {
            if (b.GetUserData() != null) {
                //Synchronize the AtlasSprites position and rotation with the corresponding body
                var myActor = b.GetUserData();
                myActor.setPosition(cc.p(b.GetPosition().x * PTM_RATIO, b.GetPosition().y * PTM_RATIO));
//                myActor.setRotation(-1 * cc.RADIANS_TO_DEGREES(b.GetAngle()));
            }
        }

		//Iterate over all pair of bodies that made contact
		var event = this.world.GetContactList();
		while (event) {
            
			var bodyA = event.GetFixtureA().GetBody();
			var bodyB = event.GetFixtureB().GetBody();
            var spriteA = bodyA.GetUserData();
            var spriteB = bodyB.GetUserData();

			//Player vs Items
			if (spriteA.tag == 2 && spriteB.tag == 1) {
				spriteA.combineColor( spriteB.getColor() );
				this.world.DestroyBody(bodyB);
				spriteB.setVisible(false);
			}
			else if (spriteA.tag == 1 && spriteB.tag == 2) {
				spriteB.combineColor( spriteA.getColor() );
				this.world.DestroyBody(bodyA);
				spriteA.setVisible(false);
			}
			//Player vs Enemy
			else if (spriteA.tag == 2 && spriteB.tag == 3) {
				spriteA.damage();
			}
			else if (spriteA.tag == 3 && spriteB.tag == 2) {
				spriteB.damage();
			}
                
            event = event.next;
        }		
    },
	movingBackground:function () {
		//Last Background
        this.spriteBG.runAction(cc.MoveBy.create(TIME_TO_ANIM, cc.p(-this.winSize.width/8, 0)));
        this.deltaAnchorBGWidth -= this.winSize.width/8;

        if (this.deltaAnchorBGWidth <= (9 * this.winSize.width/8) ) {
            if (!this.isBGReload) {
                this.anotherBG = new BG("res/HelloWorld.png");
				this.anotherBG.setAnchorPoint(cc.p(0,0));
                this.anotherBG.setPosition(this.winSize.width, 0);
				this.addChild(this.anotherBG, 1);
                this.isBGReload = true;
            }
            this.anotherBG.runAction(cc.MoveBy.create(TIME_TO_ANIM, cc.p(-this.winSize.width/8, 0)));
        }
		if (this.deltaAnchorBGWidth <= this.winSize.width/8 ) {
            this.deltaAnchorBGWidth = this.spriteBG.getContentSize().width;
            this.spriteBG.destroy();
            this.spriteBG = this.anotherBG;
            this.anotherBG = null;
            this.isBGReload = false;
        }

		//Hills Background
		this.hillsBG.updateHills( -this.winSize.width/4, TIME_TO_ANIM );
    },
});

var GameScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
		
		var layer = new GameLayer();
		var HUDLayer = new GameHUD();

	    layer.init();
		HUDLayer.init();

	    this.addChild(layer);
		this.addChild(HUDLayer); 
    }
});
