var s_HelloWorld = "res/HelloWorld.png";
var s_CloseNormal = "res/CloseNormal.png";
var s_CloseSelected = "res/CloseSelected.png";
var s_Player = "res/Piratey_transparent_background.png";
var s_PlayerColor = "res/water_color.png";
var s_Square = "res/square.png";

var g_resources = [
    //image
    {src:s_HelloWorld},
    {src:s_CloseNormal},
    {src:s_CloseSelected},
	{src:s_Player},
	{src:s_PlayerColor},
	{src:s_Square}

    //plist

    //fnt

    //tmx

    //bgm

    //effect
];
