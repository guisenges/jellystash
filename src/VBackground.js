var PTM_RATIO = 32;

var VBackground = cc.Sprite.extend({
	//Box2D
	b2Vec2:Box2D.Common.Math.b2Vec2,
	b2BodyDef:Box2D.Dynamics.b2BodyDef,
	b2Body:Box2D.Dynamics.b2Body,
	b2FixtureDef:Box2D.Dynamics.b2FixtureDef,
	b2PolyShape:Box2D.Collision.Shapes.b2PolygonShape,

	lowerHeightPoints:{},
	upperHeightPoints:{},
	winSize:null,
	numOfHeights:null,
	drawN:null,
	renderedHeights:null,
	myLowerBody:null,
	myUpperBody:null,
	myWorld:null,
	minHeightsOnScreen:null,
	deltaPos:0,

    ctor:function (world, deltaToAnim) {
        this._super();

		this.myWorld = world;
		
		this.numOfHeights = 1000;
		this.renderedHeights = 6;
		this.minHeightsOnScreen = 5;
		this.winSize = cc.Director.getInstance().getWinSize();
		this.generateHeights(deltaToAnim);

		this.drawN = cc.DrawNode.create();
        this.addChild( this.drawN, 1 );
		this.drawN.setAnchorPoint(cc.p(0,0));
    },
	destroy:function () {
        this.setVisible(false);
    },
	generateHeights:function (deltaToAnim) {
		var w = deltaToAnim;
	
		for (var i=0; i < this.numOfHeights; i++) {
			//Lower heights
			var rand = Math.random();
			var h = (this.winSize.height/2.5) * rand;
			this.lowerHeightPoints[i] = cc.p(w, h);

			//Upper heights
			rand = Math.random();
			h = this.winSize.height - ((this.winSize.height/2.5) * rand);
			this.upperHeightPoints[i] = cc.p(w, h);
			w += deltaToAnim;
		}
	},
	updateHills: function (deltaPosition, timeToAnim) {
		this.deltaPos += deltaPosition;

		if( Math.abs(this.deltaPos) > this.winSize.width/10 ) {

			if( this.myLowerBody ) {
				this.myWorld.DestroyBody(this.myLowerBody);
				this.myLowerBody = null;
				this.drawN.clear();
			}
			if( this.myUpperBody ) {
				this.myWorld.DestroyBody(this.myUpperBody);
				this.myUpperBody = null;
				this.drawN.clear();
			}

			this.updateBodyAndDraw(deltaPosition, this.deltaPos, timeToAnim);
		}
	},
	updateBodyAndDraw: function (deltaPosition, totalMoved, timeToAnim) {
		var velocity = deltaPosition/(PTM_RATIO * timeToAnim)

		var lowerBodyDef = new this.b2BodyDef;
		lowerBodyDef.position.Set(totalMoved/PTM_RATIO,0);
		lowerBodyDef.type = this.b2Body.b2_kinematicBody;
		lowerBodyDef.userData = this;
		this.myLowerBody = this.myWorld.CreateBody(lowerBodyDef);
		this.myLowerBody.SetLinearVelocity(new this.b2Vec2(velocity,0));

		var upperBodyDef = new this.b2BodyDef;
		upperBodyDef.position.Set(totalMoved/PTM_RATIO,0);
		upperBodyDef.type = this.b2Body.b2_kinematicBody;
		upperBodyDef.userData = this;
		this.myUpperBody = this.myWorld.CreateBody(upperBodyDef);
		this.myUpperBody.SetLinearVelocity(new this.b2Vec2(velocity,0));

		var fixLowerHeightsDef = new this.b2FixtureDef;
        fixLowerHeightsDef.density = 1.0;
        fixLowerHeightsDef.friction = 0.0;
        fixLowerHeightsDef.restitution = 0.0;
        fixLowerHeightsDef.shape = new this.b2PolyShape;

		var fixUpperHeightsDef = new this.b2FixtureDef;
        fixUpperHeightsDef.density = 1.0;
        fixUpperHeightsDef.friction = 0.0;
        fixUpperHeightsDef.restitution = 0.0;
        fixUpperHeightsDef.shape = new this.b2PolyShape;

		for (var i=(this.renderedHeights - this.minHeightsOnScreen); i < this.renderedHeights; i++) {
			this.drawN.drawSegment( this.lowerHeightPoints[i-1], this.lowerHeightPoints[i], 2, cc.c4f(0, 1, 0, 1) );
			this.drawN.drawSegment( this.upperHeightPoints[i-1], this.upperHeightPoints[i], 2, cc.c4f(1, 0, 0, 1) );
			
			var pL0 = new this.b2Vec2(this.lowerHeightPoints[i-1].x/PTM_RATIO,this.lowerHeightPoints[i-1].y/PTM_RATIO);
			var pL1 = new this.b2Vec2(this.lowerHeightPoints[i].x/PTM_RATIO,this.lowerHeightPoints[i].y/PTM_RATIO);
			fixLowerHeightsDef.shape.SetAsArray([pL0,pL1],2);
			this.myLowerBody.CreateFixture(fixLowerHeightsDef);

			var pU0 = new this.b2Vec2(this.upperHeightPoints[i-1].x/PTM_RATIO,this.upperHeightPoints[i-1].y/PTM_RATIO);
			var pU1 = new this.b2Vec2(this.upperHeightPoints[i].x/PTM_RATIO,this.upperHeightPoints[i].y/PTM_RATIO);
			fixUpperHeightsDef.shape.SetAsArray([pU0,pU1],2);
			this.myUpperBody.CreateFixture(fixUpperHeightsDef);
		}
		this.renderedHeights += 1;
	},
});
