PTM_RATIO = 32;

var ColorItem = cc.Sprite.extend({
	//Box2D
	b2Vec2:Box2D.Common.Math.b2Vec2,
	b2BodyDef:Box2D.Dynamics.b2BodyDef,
	b2Body:Box2D.Dynamics.b2Body,
	b2FixtureDef:Box2D.Dynamics.b2FixtureDef,
	b2PolyShape:Box2D.Collision.Shapes.b2PolygonShape,

	winSize:null,
	colorIdx:0,
	body:null,
	myWorld:null,

    ctor:function (world) {
        this._super();

		this.initWithFile("res/square.png");
		this.tag = 1;
		this.winSize = cc.Director.getInstance().getWinSize();
		this.myWorld = world;

		var bodyDef = new this.b2BodyDef;
		bodyDef.position.Set(this.winSize.width/PTM_RATIO, this.winSize.height/(2.5 * PTM_RATIO));
		bodyDef.type = this.b2Body.b2_kinematicBody;
		bodyDef.userData = this;
		this.body = this.myWorld.CreateBody(bodyDef);
		this.body.SetLinearVelocity(new this.b2Vec2(-3,0));

		// Define another box shape for our dynamic body.
        var dynamicBox = new this.b2PolyShape();
        dynamicBox.SetAsBox(0.5, 0.5);//These are mid points for our 1m box

        // Define the dynamic body fixture.
        var fixtureDef = new this.b2FixtureDef();
        fixtureDef.shape = dynamicBox;
        fixtureDef.density = 1.0;
        fixtureDef.friction = 0.3;
        this.body.CreateFixture(fixtureDef);
    },
	sortColor:function () {
		this.colorIdx = Math.floor((Math.random()*3)+1);
		if (this.colorIdx == 1) {
			this.setColor(cc.magenta());
		}
		else if(this.colorIdx == 2) {
			this.setColor(cc.c4(0,255,255,255));
		}
		else if(this.colorIdx == 3) {
			this.setColor(cc.yellow());
		}
	},
});
