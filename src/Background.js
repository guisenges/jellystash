var BG = cc.Sprite.extend({
    ctor:function (imgPath) {
        this._super();
		this.initWithFile(String(imgPath));
    },
	destroy:function () {
        this.setVisible(false);
    }
});
