var PTM_RATIO = 32;

var Player = cc.Sprite.extend({
	//Box2D
	b2Vec2:Box2D.Common.Math.b2Vec2,
	b2BodyDef:Box2D.Dynamics.b2BodyDef,
	b2Body:Box2D.Dynamics.b2Body,
	b2FixtureDef:Box2D.Dynamics.b2FixtureDef,
	b2PolyShape:Box2D.Collision.Shapes.b2PolygonShape,

	speed:5,
	winSize:null,
	colorIdx:0,
	skinColor:null,
	isHurt:false,
	body:null,
	myWorld:null,

    ctor:function (world) {
        this._super();

		this.myWorld = world;
		CONTAINER.CREATURE = [];

		this.initWithFile("res/square.png");
		this.skinColor = cc.Sprite.create("res/water_color.png");
		this.winSize = cc.Director.getInstance().getWinSize();
		this.skinColor.setPosition(cc.p(this.getContentSize().width/2, this.getContentSize().height/2));
		this.skinColor.setColor(cc.magenta());
		this.addChild(this.skinColor,1);
		this.tag = 2;

		var bodyDef = new this.b2BodyDef();
        bodyDef.type = this.b2Body.b2_dynamicBody;
        bodyDef.position.Set(this.winSize.width/(6 * PTM_RATIO), this.winSize.height/(2.5 * PTM_RATIO) );
        bodyDef.userData = this;
        this.body = world.CreateBody(bodyDef);

        // Define another box shape for our dynamic body.
        var dynamicBox = new this.b2PolyShape();
        dynamicBox.SetAsBox(0.5, 0.5);//These are mid points for our 1m box

        // Define the dynamic body fixture.
        var fixtureDef = new this.b2FixtureDef();
        fixtureDef.shape = dynamicBox;
        fixtureDef.density = 1.0;
        fixtureDef.friction = 0.3;
        this.body.CreateFixture(fixtureDef);

    },
    handleKey:function (e) {
        if ((e == cc.KEY.w || e == cc.KEY.up)) {
			this.body.SetActive(true);
			var f = new this.b2Vec2(0,4);
    		this.body.ApplyImpulse(f, this.body.GetPosition());
        }
	},
	damage:function () {
		if(!this.isHurt) {
			this.isHurt = true;
			this.runAction(cc.Sequence.create(	cc.Blink.create( 2, 5 ),
												cc.CallFunc.create(this.enableHarm, this) ) );
		}
	},
	combineColor:function (color) {
		this.skinColor.setColor(cc.c4(	this.skinColor.getColor().r + color.r,
										this.skinColor.getColor().g + color.g,
										this.skinColor.getColor().b + color.b,
										255));
	},
	enableHarm:function (target) {
		this.isHurt = false;
	},
});
